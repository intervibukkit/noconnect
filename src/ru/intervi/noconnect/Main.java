package ru.intervi.noconnect;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.net.ProxySelector;

public class Main extends JavaPlugin implements Listener {
	public Config conf = new Config(this);
	private Core core = new Core(this, ProxySelector.getDefault());
	
	@Override
	public void onEnable() {
		conf.load();
		if (conf.enable) ProxySelector.setDefault(core);
	}
	
	@Override
	public void onDisable() {
		ProxySelector.setDefault(core.getProxy());
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		conf.load();
		if (conf.enable) ProxySelector.setDefault(core);
		else ProxySelector.setDefault(core.getProxy());
		sender.sendMessage("[NoConnect] config reloaded");
		return true;
	}
}
