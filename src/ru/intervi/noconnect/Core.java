package ru.intervi.noconnect;

import java.net.ProxySelector;
import java.net.URI;
import java.net.SocketAddress;
import java.io.IOException;
import java.net.Proxy;
import java.util.List;

public class Core extends ProxySelector {
	public Core(Main main, ProxySelector proxy) {
		this.main = main;
		this.proxy = proxy;
	}
	
	private Main main;
	private ProxySelector proxy;
	
	@Override
	public List<Proxy> select(URI uri) {
		if (main.conf.enable) {
			if (!main.conf.whitelist.isEmpty() && main.conf.whitelist.contains(uri.getHost())) {
				if (main.conf.log) main.getLogger().info(uri.getHost() + " allowed");
				return proxy.select(uri);
			}
			if (main.conf.log) main.getLogger().info(uri.getHost() + " interrupt");
			new IOException("[NoConnection] not allowed");
		}
		return proxy.select(uri);
	}
	
	@Override
	public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
		proxy.connectFailed(uri, sa, ioe);
	}
	
	public ProxySelector getProxy() {
		return proxy;
	}
}
