package ru.intervi.noconnect;

import java.util.List;
import java.util.ArrayList;

import org.bukkit.configuration.file.FileConfiguration;

public class Config {
	public Config(Main main) {
		this.main = main;
	}
	
	private Main main;
	
	public boolean enable = true;
	public boolean log = false;
	public List<String> whitelist = new ArrayList<String>();
	
	public void load() {
		main.saveDefaultConfig();
		main.reloadConfig();
		FileConfiguration conf = main.getConfig();
		enable = conf.getBoolean("enable", true);
		log = conf.getBoolean("log", false);
		whitelist = conf.getStringList("whitelist");
	}
}
